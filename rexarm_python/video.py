import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])
        self.data = np.array([])
        self.distort = np.array([])
        self.w=640*2
        self.h=480*2
        self.cal_flag = False
        self.gray_frame=np.array([])
        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 5                                     # Change!
        self.real_coord = np.float32([[0., 0.], [305.,-305.], [-305.,-305.],[-305.,305.],[305.,305.]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32((2,3))
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret == True):
            rgb_frame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
            if (self.cal_flag == True):
                #self.loadCalibration()
                #rgb_frame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
                self.gray_frame=cv2.cvtColor(rgb_frame,cv2.COLOR_BGR2GRAY)
            #self.loadCalibration()
                new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.data,self.distort,(self.w,self.h),1,(self.w,self.h))
                self.currentFrame = cv2.undistort(rgb_frame, self.data, self.distort, None, new_camera_matrix)
            else:
                #rgb_frame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
                self.gray_frame=cv2.cvtColor(rgb_frame,cv2.COLOR_BGR2GRAY)
                self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
            #self.loadCalibration()
            #new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.data,self.distort,(self.w,self.h),1,(self.w,self.h))
            #self.currentFrame = cv2.undistort(rgb_frame, self.data, self.distort, None, new_camera_matrix)
    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
        self.data = np.load("./rexarm_python/calibration.npy")
        self.distort = np.load("./rexarm_python/distort.npy")
        self.cal_flag = True
        pass

