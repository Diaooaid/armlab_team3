import sys
import cv2
import time
import math
import numpy as np
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
from scipy.signal import butter, lfilter,filtfilt
from video import Video
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510
 
class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])
        self.waypoint = np.array([])
        self.tr_flag=False
        self.wp_count=0
        self.run_flag=False
        self.feedback_path=np.array([])
        self.feedback_time=np.array([])
        self.full_path = np.array([])
        self.world=np.array([])
        self.prev_click=np.float32([0,0])
        self.temp=np.array([])
        self.temp_w=0
        self.temp_h=0
        self.target_loc_x=np.array([])
        self.target_loc_y=np.array([])
        self.target_path=np.array([])
        self.exec_path_flag=False
        self.smooth_path=np.array([])
        self.curve_fb_path=np.array([]) 
        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()
        """
        Creates a timer to call tr_playback to check if waypoint is reached
        """
        self._timer3 = QtCore.QTimer(self)
        #self._timer3.start(27)
        
        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnLoadPlan.clicked.connect(self.loadplan)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.playback_timer)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.locate)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)


    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))
        """
        Update world coord of arm tip in GUI
        """
        mat,mat1=self.world_coord()
        self.world=np.dot(mat,np.reshape(np.array([0,0,0,1]),(4,1)))
        wrist= np.dot(mat1,np.reshape(np.array([0,0,0,1]),(4,1)))
        #self.world[2]=self.world[0]+121.0
        #self.world[0]=self.world[1]*math.cos(self.rex.joint_angles_fb[0])
        #self.world[1]=self.world[1]*math.sin(self.rex.joint_angles_fb[0])
        
        #print self.world
        #print self.world[0]
        phi=math.atan((wrist[2]-self.world[2])/math.sqrt((wrist[0]-self.world[0])**2+(wrist[1]-self.world[1])**2))
        self.ui.rdoutX.setText(str(round(self.world[0],4)))
        self.ui.rdoutY.setText(str(round(self.world[1],4)))
        self.ui.rdoutZ.setText(str(round(self.world[2],4)))
        self.ui.rdoutT.setText(str(round(phi*R2D,4)))
        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
      
        
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("%d,%d" % (x,y))
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                """ TO DO Here is where affine calibration must be used """ 
               
                a = self.video.aff_matrix[0][0]
                b = self.video.aff_matrix[0][1]
                c = self.video.aff_matrix[0][2]
                d = self.video.aff_matrix[1][0]
                e = self.video.aff_matrix[1][1]
                f = self.video.aff_matrix[1][2] 
                x_world = a*x+b*y+c
                y_world = d*x+e*y+f 
                self.ui.rdoutMouseWorld.setText("%.0f,%.0f" % (x_world,y_world))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))


    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()))
        if(self.tr_flag==False):
            self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        else:
            self.rex.max_torque = 0    
        self.rex.speed = self.ui.sldrSpeed.value()/100.0
        self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
        self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
        self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
        self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R
        self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        
        self.prev_click[0]=self.last_click[0]
        self.prev_click[1]=self.last_click[1]
          
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y
        
        
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                #print("affine done!")
                """ Perform affine calibration with OpenCV """
                #self.video.aff_matrix = cv2.getAffineTransform(
                #                        self.video.mouse_coord,
                #                        self.video.real_coord)
                self.affine()
                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
                print self.video.aff_matrix

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        print "Load Camera Cal"
        self.video.loadCalibration()
    def affine(self):
        b = np.reshape(self.video.real_coord, (2*len(self.video.real_coord),1))
        A = np.zeros((2*len(self.video.mouse_coord),6))
        for i in range(len(self.video.mouse_coord)):
        
            row1 = np.append(self.video.mouse_coord[i],[1,0,0,0])
            row2 = np.append([0,0,0], self.video.mouse_coord[i])
            row2 = np.append(row2, [1])       
            A[2*i] = row1
            A[2*i+1] = row2
        inv = np.linalg.pinv(A)
        x = np.dot(inv,b)
        self.video.aff_matrix = np.reshape(x, (2,3))
    def tr_initialize(self):
        print "Teach and Repeat"
        self.waypoint = np.array([])
        self.full_path = np.array([])
        self.smooth_path=np.array([]) 
        self.wp_count=0
        self.tr_flag=True
        self.slider_change()
        self.exec_path_flag=False
        
    def tr_add_waypoint(self):
        print "Add Waypoint"
        self.waypoint = np.append(self.waypoint, self.rex.joint_angles_fb)
        self.waypoint_new =np.reshape(self.waypoint,(0.25*len(self.waypoint),4))
        np.save("waypoint.npy",self.waypoint_new)
        #print self.waypoint
    def tr_smooth_path(self):
        print "Smooth Path"
        #self.tr_flag=False
        #self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        inter_coef=8.0
        self.interp=self.smooth(inter_coef,self.full_path)
        np.save("smooth_path.npy",self.interp)
    def loadplan(self):
        
        self.tr_flag=False
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        
        self.full_path = np.zeros((3*len(self.waypoint_new),4))
        for i in range(len(self.waypoint_new)):
            self.full_path[3*i]=self.waypoint_new[i]-[0.0,8.0*D2R,0*D2R, 0.0*D2R]
            self.full_path[3*i+1]=self.waypoint_new[i]
            self.full_path[3*i+2]=self.waypoint_new[i]-[0.0,8.0*D2R,0*D2R, 0*D2R]
        #np.save("full_path.npy", self.full_path)
        #self.smooth_path=np.load("full_path.npy")
        print "plan loaded"     
    def smooth(self,inter_coef,origin):   
        #print(len(self.full_path))
        #print np.arange(0,len(self.full_path),1/inter_coef)
        #print self.full_path.transpose()
        interP=np.empty((4,inter_coef*len(origin)))
        b, a = butter(10, 0.65, btype='low', analog=False)
        
        for i in range(0,4):
            #plt.subplot(2, 1, 2)
            interP[i] = np.interp(np.arange(0,len(origin),1/inter_coef),range(len(origin)), origin.transpose()[i])
            #plt.plot(range(len(interP[i])), interP[i], 'b-', label='data')
            interP[i] = filtfilt(b,a,interP[i],padlen= 20)
            #plt.plot(range(len(self.interp[i])), self.interp[i], 'g-', linewidth=2, label='filtered data')
            #plt.show()
        return interP.transpose()
    def curve(self):
        z=np.arange(20,98,0.05)
        x=(20+50*(z-20)/78)*np.cos(z)
        y=(20+50*(z-20)/78)*np.sin(z)+150
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.plot(xs=x,ys=y,zs=z,zdir='z')
        plt.show()
        #x=2*16*(np.sin(t)**3)
        #z=2*(13*np.cos(t)-5*np.cos(2*t)-2*np.cos(3*t)-np.cos(4*t))+50
        #phi=np.array([])
        curve_path=np.array([])
        for i in range(len(z)):
            phi=self.generate_phi(x[i],y[i],z[i])
            curve_path = np.append(curve_path,self.inverse_kinematics(x[i],y[i],z[i],phi))
        curve_path=np.reshape(curve_path,(0.25*len(curve_path),4))
        np.save("3D_curve.npy",curve_path)
        return curve_path
    def playback_timer(self):
        self.run_flag =  not self.run_flag 
        print self.run_flag
        self.curve_flag = False   # require manually setting to False if not used
        if(self.run_flag == True):
            self.wp_count=0
            if (self.exec_path_flag==True):
                self.smooth_path= self.target_path
            elif(self.curve_flag==True):
                curve_path=self.curve()
                self.smooth_path=curve_path
            else:    
                self.smooth_path=self.interp
            self._timer3.timeout.connect(self.tr_playback)
            self._timer3.start(50)
        else:
            self._timer3.stop()
            if(self.curve_flag==True):
                self.curve_fb_path=np.reshape(self.curve_fb_path,(0.25*len(self.curve_fb_path),4))
                np.save("3D_fb.npy",self.curve_fb_path)
                self.curve_fb_path=np.array([]) 
            
    def tr_playback(self):
        
        #print "Playback"
        #print self.wp_count
           
        #self.full_path = np.array([])
        #oldTime=time.time()
        
        #self.rex.joint_angles=self.waypoint_new[self.wp_count]
        
        """
        if(self.wp_count==0):
            self.waypoint_new =np.reshape(self.waypoint,(0.25*len(self.waypoint),4))
            self.full_path = np.zeros((3*len(self.waypoint_new),4))
            for i in range(len(self.waypoint_new)):
                self.full_path[3*i]=self.waypoint_new[i]-[0.0,8.0*D2R,3.5*D2R, 0.0*D2R]
                self.full_path[3*i+1]=self.waypoint_new[i]
                self.full_path[3*i+2]=self.waypoint_new[i]-[0.0,8.0*D2R,3.5*D2R, 0*D2R]  
            #self.full_path = np.reshape(self.full_path, (0.25*len(self.full_path),4))
            np.save("waypoints.npy",self.waypoint_new)
        """
        if(self.wp_count==len(self.smooth_path)-1):
            self.feedback_path =np.reshape(self.feedback_path,(0.25*len(self.feedback_path),4))
            self.feedback_time =np.reshape(self.feedback_time,(0.25*len(self.feedback_time),4))
            np.save("feedback_path.npy",self.feedback_path)
            np.save("feedback_time.npy",self.feedback_time)
            self.wp_count=self.wp_count+1
        #self.smooth_path=self.waypoint_new
        if(self.wp_count<len(self.smooth_path)):
            self.rex.joint_angles=self.smooth_path[self.wp_count]
       
            self.rex.cmd_publish()
            if(np.absolute(self.rex.joint_angles_fb[0]-self.smooth_path[self.wp_count][0])<=0.05 and np.absolute(self.rex.joint_angles_fb[1]-self.smooth_path[self.wp_count][1])<=0.05 and np.absolute(self.rex.joint_angles_fb[2]-self.smooth_path[self.wp_count][2])<=0.05 and np.absolute(self.rex.joint_angles_fb[3]-self.smooth_path[self.wp_count][3])<=0.05 and self.wp_count<(len(self.smooth_path)-1)):
                self.wp_count =self.wp_count+1
                self.feedback_path=np.append(self.feedback_path,self.rex.joint_angles_fb)
                self.feedback_time=np.append(self.feedback_time,self.rex.motortime)
                if(self.curve_flag==True):
                    self.curve_fb_path=np.append(self.curve_fb_path,self.rex.joint_angles_fb)
        #newTime=time.time()
        #print newTime-oldTime                             
                    
    def def_template(self):
        print "Define Template" 
        first_click = self.prev_click
        second_click = self.last_click
        self.temp_w=int(abs(first_click[0]-second_click[0]))
        self.temp_h=int(abs(first_click[1]-second_click[1]))
        self.video.gray_frame = cv2.equalizeHist(self.video.gray_frame)
        #self.video.gray_frame = cv2.blur(self.video.gray_frame,(10,10))
        ret,self.video.gray_frame = cv2.threshold(self.video.gray_frame, 150,255,cv2.THRESH_TRUNC)
        ret,self.video.gray_frame = cv2.threshold(self.video.gray_frame, 93,255,cv2.THRESH_TOZERO)
        cv2.imwrite("smooth_equal.png", self.video.gray_frame)
        self.temp=self.video.gray_frame[2*first_click[1]:2*second_click[1],2*first_click[0]:2*second_click[0]]
        #cv2.imwrite("./rexarm_python/sample.png",self.temp)
        
        self.template_match()
    def template_match(self):
        print "Template Match"
        temp = cv2.resize(self.temp,None,fx=0.4,fy=0.4, interpolation=cv2.INTER_AREA)
        img = cv2.resize(self.video.gray_frame, None,fx=0.4,fy=0.4, interpolation=cv2.INTER_AREA)
        h,w=temp.shape
        result=np.zeros((img.shape[0]+h,img.shape[1]+w))+255
        for i in range(0,img.shape[0]+1):
            for j in range(0,img.shape[1]+1):
                window=img[i:i+h-1,j:j+w-1]
                result[i,j]=self.ssd(window,temp)
        result=result[0:img.shape[0]-h,0:img.shape[1]-w]
        result8 = cv2.normalize(result,None,0,255,cv2.NORM_MINMAX,cv2.CV_8U)        
        cv2.imwrite('./rexarm_python/result.png',result8)
        #plt.hist(result8.ravel(),256,[0,256])
        #plt.show()
        print min(result.ravel()),max(result.ravel()) 
        loc = np.where( result <= 80000)
        loc=(loc[0].astype("int"),loc[1].astype("int"))
        print len(loc[0])
        
        clean_loc=self.nms(loc)
        print clean_loc
        for pt in zip(*clean_loc[::-1]):
            cv2.rectangle(img, pt, (pt[0] + h, pt[1] + w), (0,0,0), 2)
            cv2.circle(img, (int(pt[0] + h/2), int(pt[1] + w/2)),2, (0,0,0), 2)  
        cv2.imwrite('./rexarm_python/resultrect.png',img) 
        self.target_loc_x=(clean_loc[0]+h/2)*1.25
        self.target_loc_y=(clean_loc[1]+w/2)*1.25

    def nms(self, loc):
        
        clean_loc_x=np.array([])
        clean_loc_y=np.array([])
        loc_x=loc[0]
        loc_y=loc[1]
        while(len(loc_x)>0):
            idx=[0]
            
            for i in range(len(loc_x)):
                if(((loc_x[0]-loc_x[i])**2+(loc_y[0]-loc_y[i])**2)<225):
                    idx.append(i)
            clean_loc_x=np.append(clean_loc_x, loc_x[0])
            clean_loc_y=np.append(clean_loc_y, loc_y[0])
            #idx=idx.append(0)        
            loc_x=np.delete(loc_x, idx)
            loc_y=np.delete(loc_y, idx)
        clean_loc=(clean_loc_x.astype("int"),clean_loc_y.astype("int"))
        return clean_loc                    
    def ssd(self,window,temp):
        width=min(window.shape[1],temp.shape[1])
        height=min(window.shape[0],temp.shape[0])
        #s=np.sum((window[0:height,0:width]-temp[0:height,0:width])**2)
        s=np.linalg.norm(window[0:height,0:width]-temp[0:height,0:width])**2
        return s
    def locate(self):
        print "Locate targets"
        a = self.video.aff_matrix[0][0]
        b = self.video.aff_matrix[0][1]
        c = self.video.aff_matrix[0][2]
        d = self.video.aff_matrix[1][0]
        e = self.video.aff_matrix[1][1]
        f = self.video.aff_matrix[1][2] 
        x_world=np.array([])
        y_world=np.array([])
        #x_sort=np.array([])
        #y_sort=np.array([])
        #angle=np.array([])
        init_path=np.array([])
        self.target_path=np.array([])
        x_world = a*self.target_loc_y+b*self.target_loc_x+c
        y_world = d*self.target_loc_y+e*self.target_loc_x+f
        z=20
        angle=np.zeros(len(x_world))
        x_sort=np.zeros(len(x_world))
        y_sort=np.zeros(len(x_world))        
        indx=np.argsort(x_world)
        for i in range(len(x_world)):
            angle[i]=math.atan2(-y_world[i],-x_world[i])
        indx=np.argsort(angle)
        for i in range(len(x_world)):
            x_sort[i]=x_world[indx[i]]
            y_sort[i]=y_world[indx[i]]             
        print angle ,angle[indx]
        print x_sort
        for i in range(len(x_world)):
            phi=self.generate_phi(x_sort[i],y_sort[i],z)
            phi2=self.generate_phi(x_sort[i]-10,y_sort[i]-10,z+40)
            #-----#
            #print phi, r-max_r
            init_path=np.append(init_path, self.inverse_kinematics(x_sort[i]-10,y_sort[i]-10,z+40,phi2))
            init_path=np.append(init_path, self.inverse_kinematics(x_sort[i],y_sort[i],z,phi))
            init_path=np.append(init_path, self.inverse_kinematics(x_sort[i]-10,y_sort[i]-10,z+40,phi2))
            #init_path=np.append(init_path, [0,0,0,0])
        init_path=np.reshape(init_path, (0.25*len(init_path),4))  
        self.target_path=self.smooth(12.0,init_path) 
        self.target_path=self.target_path   
        #print self.target_path
       
        print "Targets located, path ready"
    def exec_path(self):
        "Execute path" 
        self.exec_path_flag=True
        self.tr_flag=False
        self.playback_timer() 
        print len(self.target_path) 
    def generate_phi(self,x,y,z):
        r=math.sqrt(x**2+y**2)
            #-----#
        max_r = math.sqrt((102.2+102.2)**2 - z**2)            
        min_r = math.sqrt(98**2 - z**2)                     #min_r and max_r depends on height z
        dr=r-max_r
        if (r>min_r and r<max_r):
            phi = 90*D2R
        elif (r<min_r):
            phi = 145*D2R
        elif (r>max_r):
            if (dr <111.58):
                phi = math.acos(dr/111.58)
            else:
                phi = math.acos(111.50/111.58) 
        return phi        
    def forward_matrix(self,a,d,alpha,theta):
        matrix= np.array([4,4])
        Ctheta=math.cos(theta)
        Stheta=math.sin(theta)
        Calpha=math.cos(alpha)
        Salpha=math.sin(alpha)
        matrix = [[Ctheta,-Stheta*Calpha,Stheta*Salpha,a*Ctheta],[Stheta,Ctheta*Calpha,-Salpha*Ctheta,Stheta*a], [0, Salpha, Calpha, d], [0, 0, 0, 1]]
        return matrix 
    def world_coord(self):
        m1= self.forward_matrix(0,121,math.pi/2,self.rex.joint_angles_fb[0])
        m2= self.forward_matrix(102.2,0,0,self.rex.joint_angles_fb[1])
        m3= self.forward_matrix(102.2,0,0,self.rex.joint_angles_fb[2])
        m4= self.forward_matrix(111.58,0,0,self.rex.joint_angles_fb[3])
        A=np.array([[math.cos(math.pi/2), -math.sin(math.pi/2),0,0],[math.sin(math.pi/2),math.cos(math.pi/2),0,0],[0,0,1,0],[0,0,0,1]])
        #forward=np.dot(np.dot(np.dot(m1,m2),m3),m4)
        #forward=m1
        forward=np.dot(np.dot(np.dot(np.dot(m1,A),m2),m3),m4)
        forward1=np.dot(np.dot(np.dot(m1,A),m2),m3)
        #print forward
        return forward,forward1
         

    def inverse_kinematics(self,x,y,z,phi):
        x_c=math.sqrt(x**2+y**2)-111.58*math.cos(phi)
        y_c=z+111.58*math.sin(phi)-121
        c=math.sqrt(x_c**2+y_c**2)
        if(c>2*102.2):
            print "point is not reachable"
            return [0,0,0,0]
        #print x_c,y_c,c
        #print (102.2**2+102.2**2-c**2)/(2*(102.2**2))
        ctheta= (102.2**2+102.2**2-c**2)/(2*(102.2**2))
        #print "acos",math.acos(ctheta)
        if(x_c<0):
            theta2=math.pi-math.acos(ctheta)
            theta1=math.atan(x_c/y_c)-math.acos((c**2+102.2**2-102.2**2)/(2*c*102.2))
            theta3=math.pi-math.acos((c**2+102.2**2-102.2**2)/(2*c*102.2))+math.atan(y_c/x_c)+phi
        else:
            theta2=math.pi-math.acos(ctheta)
            theta1=math.atan(y_c/x_c)+math.acos((c**2+102.2**2-102.2**2)/(2*c*102.2))
            theta3=phi+math.atan(y_c/x_c)-math.acos((c**2+102.2**2-102.2**2)/(2*c*102.2))
        
        
        #print "atan",math.atan2(y,x)
        if(math.atan2(y,x)>=0):
            base=-math.pi+math.atan2(y,x)
        else:
            base=math.pi+math.atan2(y,x)
        
        #print [base*R2D,(math.pi/2-theta1)*R2D,R2D*theta2,R2D*theta3]
        sho=math.pi/2-theta1
        elb=theta2
        wrs=theta3
        #print sho, elb, wrs
        if(abs(base*R2D)>180. or abs(sho*R2D)>121. or abs(elb*R2D)>123. or abs(wrs*R2D)>125.):
            print "not reachable with given phi" [base, sho, elb, wrs]
            return [0,0,0,0]
        else:    
            return [base,math.pi/2-theta1,theta2,theta3]

        """
        counter=0
        if(counter==0):
            self.rex.joint_angles=[base,0,0,0]
            self.rex.cmd_publish()  
            counter=counter+1
        elif(counter==1):
            self.rex.joint_angles=[0,math.pi/2-theta1,0,0]
            self.rex.cmd_publish()
            counter=counter+1  
        elif(counter==2):
            self.rex.joint_angles=[0,0,theta2,0]
            self.rex.cmd_publish()
            counter=counter+1
        elif(counter==3):  
            self.rex.joint_angles=[0,0,0,theta3]
            self.rex.cmd_publish()
            counter=counter+1
        elif(counter==4):
            counter=0  
        """ 
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
